//
// AFHTTPRequest.h
//
// Copyright (c) 2021 QNAP Systems, Inc.
// All Rights Reserved.
//
// Developed by QNAP Systems, Inc.
//

#import <Foundation/Foundation.h>

@class AFHTTPRequest;
@class AFURLSessionManager;

typedef enum AFNetworkErrorType {
    AFConnectionFailureErrorType = 1,
    AFRequestTimedOutErrorType = 2,
    AFAuthenticationErrorType = 3,
    AFRequestCancelledErrorType = 4,
    AFUnableToCreateRequestErrorType = 5,
    AFInternalErrorWhileBuildingRequestType  = 6,
    AFInternalErrorWhileApplyingCredentialsType  = 7,
    AFFileManagementError = 8,
    AFTooMuchRedirectionErrorType = 9,
    AFUnhandledExceptionError = 10,
    AFCompressionError = 11,
} AFNetworkErrorType;

typedef void (^AFOPBasicBlock)(void);
typedef void (^AFOPHeadersBlock)(NSDictionary *responseHeaders);
typedef void (^AFOPProgressBlock)(unsigned long long size, unsigned long long total);
typedef void (^AFOPDataBlock)(NSData *data);

@protocol AFHTTPRequestDelegate <NSObject>
@optional
- (void)requestStarted:(AFHTTPRequest *)request;
- (void)request:(AFHTTPRequest *)request didReceiveResponseHeaders:(NSDictionary *)responseHeaders;
- (void)requestFinished:(AFHTTPRequest *)request;
- (void)requestFailed:(AFHTTPRequest *)request;
- (void)request:(AFHTTPRequest *)request didReceiveData:(NSData *)data;
@end

@interface AFHTTPRequest : NSOperation
{
}

+ (void)setSupportDebugLog:(BOOL)support;
+ (id)requestWithURL:(NSURL *)newURL;
+ (void)setSessionCookies:(NSMutableArray *)newSessionCookies;
+ (NSMutableArray *)sessionCookies;
+ (void)addSessionCookie:(NSHTTPCookie *)newCookie;
+ (void)clearSession;
- (void)addRequestHeader:(NSString *)header value:(NSString *)value;
- (void)appendPostData:(NSData *)data;
- (void)setPostBody:(NSMutableData*)data;
- (void)addPostValue:(NSString *)value forKey:(NSString *)key;
- (void)setPostValue:(NSString *)value forKey:(NSString *)key;
- (void)replacePostValue:(NSString *)value forKey:(NSString *)key;
- (void)addFile:(NSString *)filePath forKey:(NSString *)key;
- (void)addFile:(NSString *)filePath withFileName:(NSString *)fileName andContentType:(NSString *)contentType forKey:(NSString *)key;
- (void)setFile:(NSString *)filePath forKey:(NSString *)key;
- (void)setFile:(id)data withFileName:(NSString *)fileName andContentType:(NSString *)contentType forKey:(NSString *)key;
- (void)addData:(NSData *)data forKey:(NSString *)key;
- (void)addData:(id)data withFileName:(NSString *)fileName andContentType:(NSString *)contentType forKey:(NSString *)key;
- (void)setData:(NSData *)data forKey:(NSString *)key;
- (void)setData:(id)data withFileName:(NSString *)fileName andContentType:(NSString *)contentType forKey:(NSString *)key;
- (NSString *)responseString;
- (NSData *)responseData;
- (void)startSynchronous;
- (void)startSynchronousAbort:(int*)isAbort;
- (void)startAsynchronous;
- (void)clearDelegatesAndCancel;
- (void)setStartedBlock:(AFOPBasicBlock)aStartedBlock;
- (void)setHeadersReceivedBlock:(AFOPHeadersBlock)aHeadersReceivedBlock;
- (void)setCompletionBlock:(AFOPBasicBlock)aCompletionBlock;
- (void)setFailedBlock:(AFOPBasicBlock)aFailedBlock;
- (void)setBytesReceivedBlock:(AFOPProgressBlock)aBytesReceivedBlock;
- (void)setBytesSentBlock:(AFOPProgressBlock)aBytesSentBlock;
- (void)setDataReceivedBlock:(AFOPDataBlock)aDataReceivedBlock;

@property (nonatomic, assign) id <AFHTTPRequestDelegate> delegate;
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) NSURL *redirectURL;
@property (nonatomic, strong) NSDictionary *responseHeaders;
@property (nonatomic, strong) NSMutableDictionary *requestHeaders;
@property (nonatomic, assign) int responseStatusCode;
@property (nonatomic, strong) NSData *rawResponseData;
@property (nonatomic, strong) NSError *error;
@property (nonatomic, assign) BOOL canceledByBackgroundExpiration;

@property (nonatomic, strong) AFURLSessionManager *manager;
@property (nonatomic, strong) NSString *requestMethod;
@property (nonatomic, assign) NSTimeInterval timeOutSeconds;
@property (nonatomic, assign) unsigned long long totalBytesRead;
@property (nonatomic, assign) unsigned long long totalBytesSent;
@property (nonatomic, assign) NSStringEncoding defaultResponseEncoding;
@property (nonatomic, assign) NSStringEncoding responseEncoding;
@property (nonatomic, assign) BOOL allowCompressedResponse;
@property (nonatomic, assign) BOOL allowResumeForFileDownloads;
@property (nonatomic, assign) BOOL shouldRedirect;
@property (nonatomic, assign) BOOL validatesSecureCertificate;
@property (nonatomic, strong) NSURL *PACurl;
#if TARGET_OS_IPHONE
@property (nonatomic, assign) BOOL shouldContinueWhenAppEntersBackground;
@property (nonatomic, assign) BOOL shouldStreamPostDataFromAssetURL;
@property (nonatomic, assign) BOOL convertToJPG;
#endif
@property (nonatomic, assign) BOOL shouldStreamPostDataFromFile;
@property (nonatomic, assign) unsigned long long postDataOffset;
@property (nonatomic, assign) unsigned long long downloadOffset;
@property (nonatomic, strong) NSString *downloadDestinationPath;
@property (nonatomic, strong) NSString *userAgentString;
@property (nonatomic, strong) NSString *proxyUsername;
@property (nonatomic, strong) NSString *proxyPassword;
@property (nonatomic, strong) NSString *proxyHost;
@property (nonatomic, assign) int proxyPort;

// Used for check server status:
// if server logging in: need to wait until login process finished.
@property (nonatomic, strong) NSString *serverUID;
// Used for replace request information(port/sid/...)
@property (nonatomic, strong) NSDictionary *replaceParamDict;

@end
