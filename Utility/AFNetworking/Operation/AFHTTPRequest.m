//
// AFHTTPRequest.m
//
// Copyright (c) 2021 QNAP Systems, Inc.
// All Rights Reserved.
//
// Developed by QNAP Systems, Inc.
//

#import "AFHTTPRequest.h"
#import <objc/runtime.h>
#import <arpa/inet.h>
#if TARGET_OS_IPHONE
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import "NSData+ImageConvert.h"
#import "PHAsset+Utility.h"
#endif
#import "AFNetworking.h"

NSString* const AFNetworkRequestErrorDomain = @"AFHTTPRequestErrorDomain";
static NSString *AFHTTPRequestRunLoopMode = @"AFHTTPRequestRunLoopMode";
static NSMutableArray *sessionCookies = nil;
static NSOperationQueue *sharedQueue = nil;
static BOOL supportDebugLog = NO;

#define AF_DEBUG_LOG(fmt, ...) { if (supportDebugLog) { NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__); }}

@interface AFHTTPRequest()
{
#if TARGET_OS_IPHONE
    UIBackgroundTaskIdentifier backgroundTask;
#endif
    
    BOOL isSynchronous;
}
@property (nonatomic, getter = isFinished, readwrite)  BOOL finished;
@property (nonatomic, getter = isExecuting, readwrite) BOOL executing;
@property (nonatomic, strong) NSURLSessionTask *task;
@property (nonatomic, strong) NSMutableData *postBodyData;
@property (nonatomic, strong) NSMutableArray *postData;
@property (nonatomic, strong) NSMutableArray *fileData;
@property (nonatomic, strong) NSFileHandle *fileHandle; // for download request
@property (nonatomic, strong) AFHTTPRequest *PACFileRequest;
@property (nonatomic, copy) AFOPBasicBlock startedBlock;
@property (nonatomic, copy) AFOPHeadersBlock headersReceivedBlock;
@property (nonatomic, copy) AFOPBasicBlock completeBlock;
@property (nonatomic, copy) AFOPBasicBlock failedBlock;
@property (nonatomic, copy) AFOPProgressBlock bytesReceivedBlock;
@property (nonatomic, copy) AFOPProgressBlock bytesSentBlock;
@property (nonatomic, copy) AFOPDataBlock dataReceivedBlock;
@end

@implementation AFHTTPRequest

@synthesize finished  = _finished;
@synthesize executing = _executing;
@synthesize startedBlock = _startedBlock;
@synthesize headersReceivedBlock = _headersReceivedBlock;
@synthesize completeBlock = _completeBlock;
@synthesize failedBlock = _failedBlock;
@synthesize bytesReceivedBlock = _bytesReceivedBlock;
@synthesize bytesSentBlock = _bytesSentBlock;
@synthesize dataReceivedBlock = _dataReceivedBlock;

- (void)dealloc
{
    AF_DEBUG_LOG(@"dealloc url request [request:%p]", self);
    
    self.PACurl = nil;
    self.url = nil;
    self.responseHeaders = nil;
    self.requestHeaders = nil;
    self.rawResponseData = nil;
    self.error = nil;
    self.requestMethod = nil;
    self.downloadDestinationPath = nil;
    self.serverUID = nil;
    self.replaceParamDict = nil;
    self.startedBlock = nil;
    self.headersReceivedBlock = nil;
    self.completeBlock = nil;
    self.failedBlock = nil;
    self.bytesReceivedBlock = nil;
    self.bytesSentBlock = nil;
    self.dataReceivedBlock = nil;
    
    self.task = nil;
    self.manager = nil;
    self.postData = nil;
    self.fileData = nil;
    self.fileHandle = nil;
    
    self.delegate = nil;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _finished  = NO;
        _executing = NO;
    }
    return self;
}
         
- (void)completeOperation
{
    self.startedBlock = nil;
    self.headersReceivedBlock = nil;
    self.completeBlock = nil;
    self.failedBlock = nil;
    self.bytesReceivedBlock = nil;
    self.bytesSentBlock = nil;
    self.dataReceivedBlock = nil;
    
    self.task = nil;
    [self.manager invalidateSessionCancelingTasks:NO resetSession:YES];
    self.manager = nil;
    
    self.delegate = nil;

#if TARGET_OS_IPHONE
    if ([self shouldContinueWhenAppEntersBackground])
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (backgroundTask != UIBackgroundTaskInvalid) {
                [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
                backgroundTask = UIBackgroundTaskInvalid;
            }
        });
    }
#endif
    
    [self willChangeValueForKey:@"isExecuting"];
    [self willChangeValueForKey:@"isFinished"];

    _executing = NO;
    _finished  = YES;

    [self didChangeValueForKey:@"isExecuting"];
    [self didChangeValueForKey:@"isFinished"];
}

- (BOOL)isValidIPv6Address:(NSString*)ip
{
    NSString *tempIPAddress = [NSString stringWithString:ip];
    
    // check [ & ]
    if ([tempIPAddress hasPrefix:@"["] == YES)
    {
        // remove "["
        tempIPAddress = [tempIPAddress substringFromIndex:1];
    }
    if ([tempIPAddress hasSuffix:@"]"] == YES)
    {
        NSUInteger len = tempIPAddress.length;
        if (len > 1)
        {
            // remove "]"
            tempIPAddress = [tempIPAddress substringToIndex:len-2];
        }
    }
    
    const char *utf8 = [tempIPAddress UTF8String];
    
    struct in6_addr dst6;
    int success = inet_pton(AF_INET6, utf8, &dst6);
    
    return (success == 1);
}

- (void)parseMimeType:(NSString **)mimeType andResponseEncoding:(NSStringEncoding *)stringEncoding fromContentType:(NSString *)contentType
{
    if (!contentType) {
        return;
    }
    NSScanner *charsetScanner = [NSScanner scannerWithString: contentType];
    if (![charsetScanner scanUpToString:@";" intoString:mimeType] || [charsetScanner scanLocation] == [contentType length]) {
        *mimeType = [contentType stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        return;
    }
    *mimeType = [*mimeType stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *charsetSeparator = @"charset=";
    NSString *IANAEncoding = nil;

    if ([charsetScanner scanUpToString: charsetSeparator intoString: NULL] && [charsetScanner scanLocation] < [contentType length]) {
        [charsetScanner setScanLocation: [charsetScanner scanLocation] + [charsetSeparator length]];
        [charsetScanner scanUpToString: @";" intoString: &IANAEncoding];
    }

    if (IANAEncoding) {
        CFStringEncoding cfEncoding = CFStringConvertIANACharSetNameToEncoding((CFStringRef)IANAEncoding);
        if (cfEncoding != kCFStringEncodingInvalidId) {
            *stringEncoding = CFStringConvertEncodingToNSStringEncoding(cfEncoding);
        }
    }
}

- (void)parseStringEncodingFromHeaders
{
    // handle response text encoding
    NSStringEncoding charset = 0;
    NSString *mimeType = nil;
    [self parseMimeType:&mimeType andResponseEncoding:&charset fromContentType:[[self responseHeaders] valueForKey:@"Content-Type"]];
    if (charset != 0)
    {
        [self setResponseEncoding:charset];
    }
    else
    {
        [self setResponseEncoding:[self defaultResponseEncoding]];
    }
}

- (void)buildRequestFailed:(AFNetworkErrorType)type info:(NSDictionary*)info
{
    self.error = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:type userInfo:info];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(requestFailed:)]) {
        [self.delegate performSelector:@selector(requestFailed:) withObject:self];
    }
    else if (self.failedBlock)
    {
        self.failedBlock();
    }
    
    [self completeOperation];
}

- (NSError*)mappingError:(NSError*)urlError
{
    NSError *mappingError = self.error;
    if (!mappingError)
    {
        if (self.responseStatusCode == 407)
        {
            // auth fail
            mappingError = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:AFAuthenticationErrorType userInfo:urlError?urlError.userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"proxy authentication required"], NSLocalizedDescriptionKey, nil]];
        }
        else
        {
            if (urlError)
            {
                AFNetworkErrorType type = AFConnectionFailureErrorType;
                if (urlError.code == NSURLErrorCancelled)
                {
                    type = AFRequestCancelledErrorType;
                }
                else if (urlError.code == NSURLErrorTimedOut)
                {
                    type = AFRequestTimedOutErrorType;
                }
                else if (urlError.code == kCFStreamErrorHTTPSProxyFailureUnexpectedResponseToCONNECTMethod)
                {
                    type = AFAuthenticationErrorType;
                }
                mappingError = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:type userInfo:urlError.userInfo];
            }
        }
    }
    return mappingError;
}

- (void)startRequest
{
    AFHTTPRequest * __weak tempSelf = self;
    
    AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
    NSError *serializationError = nil;
    NSMutableURLRequest *request = nil;
    if (self.fileData.count)
    {
        request = [requestSerializer multipartFormRequestWithMethod:self.requestMethod URLString:[self.url absoluteString] parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
            AF_DEBUG_LOG(@"setup multipart info [request:%p]", tempSelf);
            
            for (NSDictionary *postInfo in tempSelf.postData)
            {
                [formData appendPartWithFormData:[postInfo objectForKey:@"value"] name:[postInfo objectForKey:@"key"]];
            }
            for (NSMutableDictionary *fileInfo in tempSelf.fileData)
            {
                NSString *fileName = [fileInfo objectForKey:@"fileName"];
                NSString *contentType = [fileInfo objectForKey:@"contentType"];
                NSString *key = [fileInfo objectForKey:@"key"];
                
                id data = [fileInfo objectForKey:@"data"];
                if ([data isKindOfClass:[NSString class]])
                {
                    NSString *sourcePath = (NSString*)data;
#if TARGET_OS_IPHONE
                    if (tempSelf.shouldStreamPostDataFromAssetURL)
                    {
                        [tempSelf appendPostDataFromPHAssetUID:sourcePath formData:formData key:key fileName:fileName contentType:contentType];
                    }
                    else
#endif
                    {
                        BOOL isDirectory = NO;
                        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:sourcePath isDirectory:&isDirectory];
                        if (!fileExists || isDirectory)
                        {
                            tempSelf.error = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:AFInternalErrorWhileBuildingRequestType userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"no file exists [path:%@]", sourcePath], NSLocalizedDescriptionKey, nil]];
                            break;
                        }
                        else
                        {
                            NSInputStream *fileInputStream = [[NSInputStream alloc] initWithFileAtPath:sourcePath];
                            [fileInputStream setProperty:[NSNumber numberWithUnsignedLongLong:tempSelf.postDataOffset] forKey:NSStreamFileCurrentOffsetKey];
                            NSError *attrError = nil;
                            NSDictionary *fileInfo = [[NSFileManager defaultManager] attributesOfItemAtPath:sourcePath error:&attrError];
                            if (attrError)
                            {
                                tempSelf.error = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:AFFileManagementError userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"failed to get file attributes [path:%@]", sourcePath], NSLocalizedDescriptionKey, nil]];
                                break;
                            }
                            else
                            {
                                long long fileSize = [[fileInfo objectForKey:NSFileSize] longLongValue]-tempSelf.postDataOffset;
                                [formData appendPartWithInputStream:fileInputStream name:key fileName:fileName length:fileSize mimeType:contentType];
                            }
                        }
                    }
                }
                else if ([data isKindOfClass:[NSData class]])
                {
                    [formData appendPartWithFileData:data name:key fileName:fileName mimeType:contentType];
                }
            }
            
            if (tempSelf.error)
            {
                [tempSelf buildRequestFailed:(int)tempSelf.error.code info:tempSelf.error.userInfo];
            }
        } error:&serializationError];
    }
    else
    {
        if (self.postData.count)
        {
            [requestSerializer setQueryStringSerializationWithBlock:^NSString * _Nonnull(NSURLRequest * _Nonnull request, id  _Nonnull parameters, NSError * _Nullable __autoreleasing * _Nullable error) {
                NSMutableArray *mutablePairs = [NSMutableArray array];
                for (NSDictionary *postInfo in tempSelf.postData)
                {
                    [mutablePairs addObject:[NSString stringWithFormat:@"%@=%@", AFPercentEscapedStringFromString([[postInfo objectForKey:@"key"] description]), AFPercentEscapedStringFromString([[[NSString alloc] initWithData:[postInfo objectForKey:@"value"] encoding:NSUTF8StringEncoding] description])]];
                }
                NSString *query = [mutablePairs componentsJoinedByString:@"&"];
                return query;
            }];
        }
        request = [requestSerializer requestWithMethod:self.requestMethod URLString:[self.url absoluteString] parameters:self.postData error:&serializationError];
    }
    
    if (!request)
    {
        [self buildRequestFailed:AFInternalErrorWhileBuildingRequestType info:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"failed to create url request [error:%@]", serializationError], NSLocalizedDescriptionKey, nil]];
        return;
    }
    
    if (self.postBodyData.length)
    {
        request.HTTPBody = self.postBodyData;
    }
    
    AF_DEBUG_LOG(@"setup url request [method:%@][url:%@][request:%p]", self.requestMethod, self.url, self);
    
    if (![self.requestHeaders objectForKey:@"Host"])
    {
        NSString *hostString = self.url.host;
        if (hostString)
        {
            NSRange tmpRange = [hostString rangeOfString:@"."];
            if (!tmpRange.length)
            {
                // check for ipv6 format
                BOOL isIPv6 = [self isValidIPv6Address:hostString];
                if (isIPv6)
                {
                    // check [ & ]
                    if ([hostString hasPrefix:@"["] == NO)
                    {
                        // add "["
                        hostString = [NSString stringWithFormat:@"[%@", hostString];
                    }
                    if ([hostString hasSuffix:@"]"] == NO)
                    {
                        // add "]"
                        hostString = [NSString stringWithFormat:@"%@]", hostString];
                    }
                }
            }
            NSNumber *portNumber = self.url.port;
            if (portNumber)
            {
                hostString = [NSString stringWithFormat:@"%@:%d", hostString, [portNumber intValue]];
            }
            [self addRequestHeader:@"Host" value:hostString];
        }
    }
    if (![self.requestHeaders objectForKey:@"User-Agent"] && self.userAgentString.length)
    {
        [self addRequestHeader:@"User-Agent" value:self.userAgentString];
    }
    if ([self allowCompressedResponse])
    {
        [self addRequestHeader:@"Accept-Encoding" value:@"gzip"];
    }
    if (self.downloadOffset)
    {
        [self addRequestHeader:@"Range" value:[NSString stringWithFormat:@"bytes=%llu-", self.downloadOffset]];
    }
    for (NSString *headerField in self.requestHeaders.keyEnumerator)
    {
        [request setValue:self.requestHeaders[headerField] forHTTPHeaderField:headerField];
    }
    
    if (!self.manager)
    {
        NSURLSessionConfiguration *sessionConfig = [NSURLSessionConfiguration defaultSessionConfiguration];
        sessionConfig.timeoutIntervalForRequest = self.timeOutSeconds;
        
        if (self.proxyHost.length && self.proxyPort)
        {
            NSDictionary *proxyInfo = @{
                (NSString *)kCFNetworkProxiesHTTPEnable:[NSNumber numberWithInt:1],
                (NSString *)kCFNetworkProxiesHTTPProxy:self.proxyHost,
                (NSString *)kCFNetworkProxiesHTTPPort:[NSNumber numberWithInt:self.proxyPort],
#if !TARGET_OS_IPHONE
                (NSString *)kCFNetworkProxiesHTTPSEnable:[NSNumber numberWithInt:1],
                (NSString *)kCFNetworkProxiesHTTPSProxy:self.proxyHost
#endif
            };
            sessionConfig.connectionProxyDictionary = proxyInfo;
        }
        
        if (self.proxyUsername.length && self.proxyPassword)
        {
            NSString *authString = [NSString stringWithFormat:@"%@:%@", self.proxyUsername, self.proxyPassword];
            NSData *authData = [authString dataUsingEncoding:NSUTF8StringEncoding];
            NSString *authHeader = [NSString stringWithFormat: @"Basic %@", [authData base64EncodedStringWithOptions:0]];
            [sessionConfig setHTTPAdditionalHeaders:@{@"Proxy-Authorization":authHeader}];
        }
        
        self.manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:sessionConfig];
        if (isSynchronous)
        {
            self.manager.completionQueue = dispatch_queue_create("AFHTTPRequestSynchronous", NULL);
        }
    }
    
    self.manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [self.manager setSessionDidReceiveAuthenticationChallengeBlock:^NSURLSessionAuthChallengeDisposition(NSURLSession * _Nonnull session, NSURLAuthenticationChallenge * _Nonnull challenge, NSURLCredential *__autoreleasing  _Nullable * _Nullable credential) {
        NSURLSessionAuthChallengeDisposition disposition = NSURLSessionAuthChallengePerformDefaultHandling;
        
        NSString *authenticationMethod = challenge.protectionSpace.authenticationMethod;
        if ([authenticationMethod isEqualToString: NSURLAuthenticationMethodServerTrust])
        {
            SecTrustRef trustObject = challenge.protectionSpace.serverTrust;
            if (self.validatesSecureCertificate == NO)
            {
                NSURLCredential *tempCredential = [NSURLCredential credentialForTrust:trustObject];
                *credential = tempCredential;
                disposition = NSURLSessionAuthChallengeUseCredential;
            }
            else
            {
                SecTrustResultType result;
                OSStatus status = SecTrustEvaluate(trustObject, &result);
                if (status == errSecSuccess && (result == kSecTrustResultProceed || result == kSecTrustResultUnspecified))
                {
                    NSURLCredential *tempCredential = [NSURLCredential credentialForTrust:trustObject];
                    *credential = tempCredential;
                    disposition = NSURLSessionAuthChallengeUseCredential;
                }
            }
        }
        AF_DEBUG_LOG(@"auth challange [method:%@][disposition:%ld][request:%p]", authenticationMethod, (long)disposition, tempSelf);
        
        return disposition;
    }];
    
    [self.manager setTaskWillPerformHTTPRedirectionBlock:^NSURLRequest * _Nonnull(NSURLSession * _Nonnull session, NSURLSessionTask * _Nonnull task, NSURLResponse * _Nonnull response, NSURLRequest * _Nonnull request) {
        AF_DEBUG_LOG(@"need to redirect? [result:%d][to url:%@][request:%p]", tempSelf.shouldRedirect, request.URL, tempSelf);
        
        // return request to follow the redirect, or return nil to stop the redirect
        NSURLRequest *urlRequest = nil;
        if (tempSelf.shouldRedirect)
        {
            tempSelf.redirectURL = request.URL;
            urlRequest = request;
        }
        return urlRequest;
    }];
    
    [self.manager setDataTaskDidReceiveResponseBlock:^NSURLSessionResponseDisposition(NSURLSession * _Nonnull session, NSURLSessionDataTask * _Nonnull dataTask, NSURLResponse * _Nonnull response) {
        if (response && [response isKindOfClass:[NSHTTPURLResponse class]])
        {
            NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse*)response;
            tempSelf.responseHeaders = urlResponse.allHeaderFields;
            tempSelf.responseStatusCode = (int)[urlResponse statusCode];
        }
        
        [tempSelf parseStringEncodingFromHeaders];
        
        if (tempSelf.delegate && [tempSelf.delegate respondsToSelector:@selector(request:didReceiveResponseHeaders:)]) {
            [tempSelf.delegate performSelector:@selector(request:didReceiveResponseHeaders:) withObject:tempSelf withObject:tempSelf.responseHeaders];
        }
        else if (tempSelf.headersReceivedBlock)
        {
            tempSelf.headersReceivedBlock(tempSelf.responseHeaders);
        }
        
        AF_DEBUG_LOG(@"receive response [status code:%d][request:%p]", tempSelf.responseStatusCode, tempSelf);
        
        if (tempSelf.downloadDestinationPath.length)
        {
            BOOL needCreateFile = NO;
            NSFileManager *manager = [NSFileManager defaultManager];
            if ([manager fileExistsAtPath:tempSelf.downloadDestinationPath])
            {
                if (tempSelf.allowResumeForFileDownloads == NO)
                {
                    BOOL result = [manager removeItemAtPath:tempSelf.downloadDestinationPath error:nil];
                    if (result)
                    {
                        needCreateFile = YES;
                    }
                }
            }
            else
            {
                needCreateFile = YES;
            }
            if (needCreateFile)
            {
                [manager createFileAtPath:tempSelf.downloadDestinationPath contents:nil attributes:nil];
            }
            tempSelf.fileHandle = [NSFileHandle fileHandleForWritingAtPath:tempSelf.downloadDestinationPath];
        }
        
        return NSURLSessionResponseAllow;
    }];
    
    [self.manager setDataTaskDidReceiveDataBlock:^(NSURLSession * _Nonnull session, NSURLSessionDataTask * _Nonnull dataTask, NSData * _Nonnull data) {
        unsigned long long size = (unsigned long)data.length;
        AF_DEBUG_LOG(@"receive data [size:%llu][request:%p]", size, tempSelf);
        
        if (tempSelf.delegate && [tempSelf.delegate respondsToSelector:@selector(request:didReceiveData:)]) {
            [tempSelf.delegate performSelector:@selector(request:didReceiveData:) withObject:tempSelf withObject:data];
        }
        else if (tempSelf.dataReceivedBlock)
        {
            tempSelf.dataReceivedBlock(data);
        }
        
        int statusCode = tempSelf.responseStatusCode;
        if (statusCode >= 200 && statusCode < 300)
        {
            unsigned long long totalSize = size;
            if (tempSelf.responseHeaders)
            {
                NSString *contentLength = [tempSelf.responseHeaders objectForKey:@"Content-Length"];
                if (contentLength)
                {
                    totalSize = (unsigned long long)[contentLength longLongValue];
                }
            }
            
            tempSelf.totalBytesRead += (unsigned long long)size;
            
            if (tempSelf.fileHandle)
            {
                [tempSelf.fileHandle seekToEndOfFile];
                [tempSelf.fileHandle writeData:data];
                
                // free AFURLSessionManagerTaskDelegate mutableData
                SEL aSelector = NSSelectorFromString(@"delegateForTask:");
                if ([tempSelf.manager respondsToSelector:aSelector])
                {
                    //id taskDelegate = [tempSelf.manager performSelector:aSelector withObject:dataTask];
                    Method method = class_getInstanceMethod([tempSelf.manager class], aSelector);
                    IMP imp = method_getImplementation(method);
                    id (*func)(id, SEL, id) = (void *)imp;
                    id taskDelegate = (id)func(tempSelf.manager, aSelector, dataTask);
                    if (taskDelegate)
                    {
                        aSelector = NSSelectorFromString(@"mutableData");
                        if ([taskDelegate respondsToSelector:aSelector])
                        {
                            //NSMutableData *mutableData = [taskDelegate performSelector:aSelector];
                            Method method = class_getInstanceMethod([taskDelegate class], aSelector);
                            IMP imp = method_getImplementation(method);
                            id (*func)(id, SEL) = (void *)imp;
                            NSMutableData *mutableData = (NSMutableData*)func(taskDelegate, aSelector);
                            [mutableData setLength:0];
                        }
                    }
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (tempSelf.bytesReceivedBlock)
                {
                    tempSelf.bytesReceivedBlock(size, totalSize);
                }
            });
        }
    }];
    
    [self.manager setTaskDidSendBodyDataBlock:^(NSURLSession *session, NSURLSessionTask *dataTask, int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        AF_DEBUG_LOG(@"[totoal:%lld][bytes sent:%lld][toto sent:%lld][request:%p]", totalBytesExpectedToSend, bytesSent, totalBytesSent, tempSelf);
        
        tempSelf.totalBytesSent += (unsigned long long)bytesSent;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if (tempSelf.bytesSentBlock)
            {
                tempSelf.bytesSentBlock(bytesSent, totalBytesExpectedToSend);
            }
        });
    }];
    
    if (self.fileData.count)
    {
        self.task = (NSURLSessionUploadTask *)[self.manager uploadTaskWithStreamedRequest:request progress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            AF_DEBUG_LOG(@"upload completion [error code:%d][status code:%d][request:%p]", error?(int)error.code:0, tempSelf.responseStatusCode, tempSelf);
            
            if (tempSelf.fileHandle)
            {
                [tempSelf.fileHandle closeFile];
                tempSelf.fileHandle = nil;
            }
            
            if (request.allHTTPHeaderFields.allKeys.count)
            {
                tempSelf.requestHeaders = [NSMutableDictionary dictionaryWithDictionary:request.allHTTPHeaderFields];
            }
            tempSelf.rawResponseData = responseObject;
            tempSelf.error = [tempSelf mappingError:error];
            
            if (tempSelf.error)
            {
                if (tempSelf.delegate && [tempSelf.delegate respondsToSelector:@selector(requestFailed:)]) {
                    [tempSelf.delegate performSelector:@selector(requestFailed:) withObject:tempSelf];
                }
                else if (tempSelf.failedBlock)
                {
                    tempSelf.failedBlock();
                }
            }
            else
            {
                if (tempSelf.delegate && [tempSelf.delegate respondsToSelector:@selector(requestFinished:)]) {
                    [tempSelf.delegate performSelector:@selector(requestFinished:) withObject:tempSelf];
                }
                else if (tempSelf.completeBlock)
                {
                    tempSelf.completeBlock();
                }
            }
            
            [tempSelf completeOperation];
        }];
    }
    else
    {
        tempSelf.task = (NSURLSessionDataTask *)[self.manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            AF_DEBUG_LOG(@"request completion [error code:%d][status code:%d][request:%p]", error?(int)error.code:0, tempSelf.responseStatusCode, tempSelf);
            
            if (tempSelf.fileHandle)
            {
                [tempSelf.fileHandle closeFile];
                tempSelf.fileHandle = nil;
            }
            
            if (request.allHTTPHeaderFields.allKeys.count)
            {
                tempSelf.requestHeaders = [NSMutableDictionary dictionaryWithDictionary:request.allHTTPHeaderFields];
            }
            tempSelf.rawResponseData = responseObject;
            tempSelf.error = [tempSelf mappingError:error];
            
            if (tempSelf.error)
            {
                if (tempSelf.delegate && [tempSelf.delegate respondsToSelector:@selector(requestFailed:)]) {
                    [tempSelf.delegate performSelector:@selector(requestFailed:) withObject:tempSelf];
                }
                else if (tempSelf.failedBlock)
                {
                    tempSelf.failedBlock();
                }
            }
            else
            {
                if (tempSelf.delegate && [tempSelf.delegate respondsToSelector:@selector(requestFinished:)]) {
                    [tempSelf.delegate performSelector:@selector(requestFinished:) withObject:tempSelf];
                }
                else if (tempSelf.completeBlock)
                {
                    tempSelf.completeBlock();
                }
            }
            
            [tempSelf completeOperation];
        }];
    }
    [self.task resume];
    
    if (tempSelf.delegate && [tempSelf.delegate respondsToSelector:@selector(requestStarted:)]) {
        [tempSelf.delegate performSelector:@selector(requestStarted:) withObject:tempSelf];
    }
    else if (self.startedBlock)
    {
        self.startedBlock();
    }
}

- (void)runPACScript:(NSString *)script
{
    if (script)
    {
        CFErrorRef err = NULL;
        NSArray *proxies = CFBridgingRelease(CFNetworkCopyProxiesForAutoConfigurationScript((__bridge CFStringRef)script,(__bridge CFURLRef)[self url], &err));
        if (!err && [proxies count] > 0)
        {
            NSDictionary *settings = [proxies objectAtIndex:0];
            [self setProxyHost:[settings objectForKey:(NSString *)kCFProxyHostNameKey]];
            [self setProxyPort:[[settings objectForKey:(NSString *)kCFProxyPortNumberKey] intValue]];
        }
    }
    
    [self startRequest];
}

- (void)fetchPACFile
{
    if ([[self PACurl] isFileURL])
    {
        NSString *pacScript = nil;
        NSStringEncoding encodingsToTry[2] = {NSUTF8StringEncoding, NSISOLatin1StringEncoding};
        NSUInteger i;
        for (i=0; i<2; i++)
        {
            pacScript = [NSString stringWithContentsOfURL:[self PACurl] encoding:encodingsToTry[i] error:NULL];
            if (pacScript.length)
            {
                break;
            }
        }
        [self runPACScript:pacScript];
        return;
    }
    
    AFHTTPRequest * __weak tempSelf = self;
    
    AFHTTPRequest *PACRequest = [AFHTTPRequest requestWithURL:[self PACurl]];
    [PACRequest setTimeOutSeconds:[self timeOutSeconds]];
    [self setPACFileRequest:PACRequest];
    [PACRequest setQueuePriority:NSOperationQueuePriorityHigh];
    __block AFHTTPRequest *tempRequest = PACRequest;
    [PACRequest setCompleteBlock:^ {
        NSString *pacScript = nil;
        NSData *data = [tempRequest responseData];
        if (data.length)
        {
            NSStringEncoding encodingsToTry[2] = {NSUTF8StringEncoding, NSISOLatin1StringEncoding};
            NSUInteger i;
            for (i=0; i<2; i++)
            {
                pacScript = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:encodingsToTry[i]];
                if (pacScript.length)
                {
                    break;
                }
            }
        }
        [tempSelf runPACScript:pacScript];
        [tempSelf setPACFileRequest:nil];
        
        [tempRequest clearDelegatesAndCancel];
        tempRequest = nil;
    }];
    [PACRequest setFailedBlock:^ {
        AF_DEBUG_LOG(@"failed to detch PAC [error:%@][request:%p]", [tempRequest error], tempSelf);
        
        if ([tempRequest error].code != AFRequestCancelledErrorType)
        {
            [tempSelf runPACScript:nil];
        }
        [tempSelf setPACFileRequest:nil];
        
        [tempRequest clearDelegatesAndCancel];
        tempRequest = nil;
    }];
    [PACRequest startAsynchronous];
}

- (void)continueMain
{
    if ([self isCancelled])
    {
        [self buildRequestFailed:AFRequestCancelledErrorType info:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"request canceled [url:%@]", self.url], NSLocalizedDescriptionKey, nil]];
        return;
    }
    
    if (!self.requestMethod)
    {
        self.requestMethod = @"GET";
    }
    if (self.postBodyData.length || self.postData.count || self.fileData.count)
    {
        if ([self.requestMethod isEqualToString:@"GET"] || [self.requestMethod isEqualToString:@"DELETE"] || [self.requestMethod isEqualToString:@"HEAD"]) {
            self.requestMethod = @"POST";
        }
    }
    
    if (self.PACurl)
    {
        [self fetchPACFile];
    }
    else
    {
        [self startRequest];
    }
}

#if TARGET_OS_IPHONE
#pragma mark - PHAsset methods

- (void)appendPostDataFromPHAsset:(PHAsset *)asset formData:(id<AFMultipartFormData>)formData key:(NSString *)key fileName:(NSString *)fileName contentType:(NSString *)contentType
{
    if (asset.mediaType == PHAssetMediaTypeImage)
    {
        BOOL isLivePhoto = NO;
        if (asset.mediaSubtypes & PHAssetMediaSubtypePhotoLive)
        {
            NSString *UTI = (__bridge_transfer NSString *)UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)[fileName pathExtension], NULL);
            NSString *MIMEType = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)UTI, kUTTagClassMIMEType);
            if (MIMEType)
            {
                if ([[MIMEType lowercaseString] hasPrefix:@"video/"])
                {
                    isLivePhoto = YES;
                }
            }
        }
        
        if (isLivePhoto == NO)
        {
            // handle image
            NSData *postImageData = nil;
            
            BOOL hasAdjustment = [asset checkForAdjustment:YES];
            if (hasAdjustment)
            {
                NSDictionary *imageDataInfo = [asset getAssetImage:fileName networkAccessAllowed:YES version:PHImageRequestOptionsVersionCurrent];
                NSData *imageData = [imageDataInfo objectForKey:@"imageData"];
                if (imageData)
                {
                    postImageData = imageData;
                }
                else
                {
                    NSDictionary *imageDataInfo = [asset getAssetImage:fileName networkAccessAllowed:YES version:PHImageRequestOptionsVersionOriginal];
                    NSData *imageData = [imageDataInfo objectForKey:@"imageData"];
                    if (imageData)
                    {
                        postImageData = imageData;
                    }
                }
            }
            else
            {
                NSDictionary *imageDataInfo = [asset getAssetImage:fileName networkAccessAllowed:YES version:PHImageRequestOptionsVersionOriginal];
                NSData *imageData = [imageDataInfo objectForKey:@"imageData"];
                if (imageData)
                {
                    postImageData = imageData;
                }
                else
                {
                    NSDictionary *imageDataInfo = [asset getAssetImage:fileName networkAccessAllowed:YES version:PHImageRequestOptionsVersionCurrent];
                    NSData *imageData = [imageDataInfo objectForKey:@"imageData"];
                    if (imageData)
                    {
                        postImageData = imageData;
                    }
                }
            }
            
            if (self.convertToJPG)
            {
                if (postImageData.length)
                {
                    // convert to jpg data
                    postImageData = [postImageData imageDataConvertToJPGData];
                }
            }
            
            if (postImageData.length)
            {
                // offset data
                NSData *subData = [postImageData subdataWithRange:NSMakeRange((NSUInteger)self.postDataOffset, (NSUInteger)([postImageData length]-self.postDataOffset))];
                [formData appendPartWithFileData:subData name:key fileName:fileName mimeType:contentType];
            }
            else
            {
                self.error = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:AFInternalErrorWhileBuildingRequestType userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"empty asset image data [asset:%@]", asset], NSLocalizedDescriptionKey, nil]];
            }
        }
        else
        {
            __block NSConditionLock *conditionLock = [[NSConditionLock alloc] initWithCondition:1];
            
            // handle live photo (video)
            PHLivePhotoRequestOptions *options = [[PHLivePhotoRequestOptions alloc] init];
            if ([options respondsToSelector:@selector(setVersion:)])
            {
                options.version = PHImageRequestOptionsVersionCurrent;
            }
            options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat;
            options.networkAccessAllowed = YES;
            
            [[PHImageManager defaultManager] requestLivePhotoForAsset:asset targetSize:CGSizeZero contentMode:PHImageContentModeAspectFill options:options resultHandler:^(PHLivePhoto *livePhoto, NSDictionary *info) {
                SEL selector = NSSelectorFromString(@"videoAsset");
                if ([livePhoto respondsToSelector:selector])
                {
                    // try to use video asset to get mov info
                    id videoAsset = [livePhoto valueForKey:@"videoAsset"];
                    if (videoAsset && [videoAsset isKindOfClass:[AVURLAsset class]])
                    {
                        NSURL *localVideoUrl = [(AVURLAsset *)videoAsset URL];
                        
                        NSString *filterString = @"file://";
                        NSString *assetURL = localVideoUrl.absoluteString;
                        NSString *assetPath = assetURL;
                        if ([assetURL hasPrefix:filterString])
                        {
                            assetPath = [assetURL substringWithRange:NSMakeRange(filterString.length, assetURL.length-filterString.length)];
                        }
                        
                        NSInputStream *fileInputStream = [[NSInputStream alloc] initWithFileAtPath:assetPath];
                        [fileInputStream setProperty:[NSNumber numberWithUnsignedLongLong:self.postDataOffset] forKey:NSStreamFileCurrentOffsetKey];
                        NSError *attrError = nil;
                        NSDictionary *fileInfo = [[NSFileManager defaultManager] attributesOfItemAtPath:assetPath error:&attrError];
                        if (attrError)
                        {
                            self.error = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:AFFileManagementError userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"failed to asset video attributes [path:%@][livePhoto:%@]", assetPath, livePhoto], NSLocalizedDescriptionKey, nil]];
                        }
                        else
                        {
                            long long fileSize = [[fileInfo objectForKey:NSFileSize] longLongValue]-self.postDataOffset;
                            [fileInputStream open];
                            
                            [formData appendPartWithInputStream:fileInputStream name:key fileName:fileName length:fileSize mimeType:contentType];
                        }
                    }
                    else
                    {
                        self.error = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:AFInternalErrorWhileBuildingRequestType userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"not AVURLAsset class [livePhoto:%@]", livePhoto], NSLocalizedDescriptionKey, nil]];
                    }
                }
                else
                {
                    self.error = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:AFInternalErrorWhileBuildingRequestType userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"no videoAsset selector [livePhoto:%@]", livePhoto], NSLocalizedDescriptionKey, nil]];
                }
                
                [conditionLock lock];
                [conditionLock unlockWithCondition:0];
            }];
            
            [conditionLock lockWhenCondition:0];
            [conditionLock unlock];
            conditionLock = nil;
        }
    }
    else if(asset.mediaType == PHAssetMediaTypeVideo)
    {
        __block NSConditionLock *conditionLock = [[NSConditionLock alloc] initWithCondition:1];
        
        PHVideoRequestOptions *options = [[PHVideoRequestOptions alloc] init];
        options.version = PHVideoRequestOptionsVersionOriginal;
        options.networkAccessAllowed = YES;
        
        [[PHImageManager defaultManager] requestAVAssetForVideo:asset options:options resultHandler:^(AVAsset *asset, AVAudioMix *audioMix, NSDictionary *info) {
            if ([asset isKindOfClass:[AVURLAsset class]])
            {
                NSURL *localVideoUrl = [(AVURLAsset *)asset URL];
                
                NSString *filterString = @"file://";
                NSString *assetURL = localVideoUrl.absoluteString;
                NSString *assetPath = assetURL;
                if ([assetURL hasPrefix:filterString])
                {
                    assetPath = [assetURL substringWithRange:NSMakeRange(filterString.length, assetURL.length-filterString.length)];
                }
                
                NSInputStream *fileInputStream = [[NSInputStream alloc] initWithFileAtPath:assetPath];
                [fileInputStream setProperty:[NSNumber numberWithUnsignedLongLong:self.postDataOffset] forKey:NSStreamFileCurrentOffsetKey];
                NSError *attrError = nil;
                NSDictionary *fileInfo = [[NSFileManager defaultManager] attributesOfItemAtPath:assetPath error:&attrError];
                if (attrError)
                {
                    self.error = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:AFFileManagementError userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"failed to asset video attributes [path:%@][asset:%@]", assetPath, asset], NSLocalizedDescriptionKey, nil]];
                }
                else
                {
                    long long fileSize = [[fileInfo objectForKey:NSFileSize] longLongValue]-self.postDataOffset;
                    [fileInputStream open];
                    
                    [formData appendPartWithInputStream:fileInputStream name:key fileName:fileName length:fileSize mimeType:contentType];
                }
            }
            else
            {
                self.error = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:AFInternalErrorWhileBuildingRequestType userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"not AVURLAsset class [asset:%@]", asset], NSLocalizedDescriptionKey, nil]];
            }
            
            [conditionLock lock];
            [conditionLock unlockWithCondition:0];
        }];
        
        [conditionLock lockWhenCondition:0];
        [conditionLock unlock];
        conditionLock = nil;
    }
    else
    {
        self.error = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:AFInternalErrorWhileBuildingRequestType userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"media type not supported [asset:%@]", asset], NSLocalizedDescriptionKey, nil]];
    }
}

- (void)appendPostDataFromPHAssetUID:(NSString *)identifier formData:(id<AFMultipartFormData>)formData key:(NSString *)key fileName:(NSString *)fileName contentType:(NSString *)contentType
{
    __block PHAsset *targetAsset = nil;
    PHFetchOptions *options = [[PHFetchOptions alloc] init];
    options.includeAllBurstAssets = YES;
    PHFetchResult* fetchResult = [PHAsset fetchAssetsWithLocalIdentifiers:@[identifier] options:options];
    if ([fetchResult count] > 0)
    {
        targetAsset = [fetchResult objectAtIndex:0];
    }
    else
    {
        // check hidden album
        PHFetchResult *albums = [PHAssetCollection fetchAssetCollectionsWithType:PHAssetCollectionTypeSmartAlbum subtype:PHAssetCollectionSubtypeSmartAlbumAllHidden options:nil];
        [albums enumerateObjectsUsingBlock:^(id object, NSUInteger idx, BOOL *stop) {
            if ([object isKindOfClass:[PHAssetCollection class]])
            {
                PHAssetCollection *collection = (PHAssetCollection *)object;
                
                PHFetchOptions *fetchOptions = [[PHFetchOptions alloc] init];
                [fetchOptions setPredicate:[NSPredicate predicateWithFormat:@"localIdentifier == %@", identifier]];
                PHFetchResult *tempAssets = [PHAsset fetchAssetsInAssetCollection:collection options:fetchOptions];
                if (tempAssets.count)
                {
                    targetAsset = [tempAssets objectAtIndex:0];
                    *stop = YES;
                }
            }
        }];
    }
    
    if (targetAsset)
    {
        [self appendPostDataFromPHAsset:targetAsset formData:formData key:key fileName:fileName contentType:contentType];
    }
    else
    {
        self.error = [NSError errorWithDomain:AFNetworkRequestErrorDomain code:AFInternalErrorWhileBuildingRequestType userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"no asset exists [id:%@]", identifier], NSLocalizedDescriptionKey, nil]];
    }
}
#endif

#pragma mark - public methods

+ (void)setSupportDebugLog:(BOOL)support
{
    supportDebugLog = support;
}

+ (id)requestWithURL:(NSURL *)newURL
{
    AFHTTPRequest *operation = [[self alloc] init];
    operation.url = newURL;
    operation.defaultResponseEncoding = NSISOLatin1StringEncoding;
    operation.allowCompressedResponse = YES;
    operation.validatesSecureCertificate = YES;
#if TARGET_OS_IPHONE
    operation.shouldContinueWhenAppEntersBackground = YES;
#endif
    
    return operation;
}

+ (void)setSessionCookies:(NSMutableArray *)newSessionCookies
{
    // remove existing cookies from the persistent store
    for (NSHTTPCookie *cookie in sessionCookies)
    {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
    
    sessionCookies = newSessionCookies;
}

+ (NSMutableArray *)sessionCookies
{
    if (!sessionCookies)
    {
        [AFHTTPRequest setSessionCookies:[NSMutableArray array]];
    }
    return sessionCookies;
}

+ (void)addSessionCookie:(NSHTTPCookie *)newCookie
{
    NSHTTPCookie *cookie;
    NSUInteger i;
    NSUInteger max = [[AFHTTPRequest sessionCookies] count];
    for (i=0; i<max; i++) {
        cookie = [[AFHTTPRequest sessionCookies] objectAtIndex:i];
        if ([[cookie domain] isEqualToString:[newCookie domain]] && [[cookie path] isEqualToString:[newCookie path]] && [[cookie name] isEqualToString:[newCookie name]]) {
            [[AFHTTPRequest sessionCookies] removeObjectAtIndex:i];
            break;
        }
    }
    [[AFHTTPRequest sessionCookies] addObject:newCookie];
}

+ (void)clearSession
{
    [[self class] setSessionCookies:nil];
}

- (void)addRequestHeader:(NSString *)header value:(NSString *)value
{
    if (!self.requestHeaders)
    {
        self.requestHeaders = [NSMutableDictionary dictionary];
    }
    [self.requestHeaders setObject:value forKey:header];
}

- (void)appendPostData:(NSData *)data
{
    if (!self.postBodyData)
    {
        self.postBodyData = [NSMutableData data];
    }
    
    [self.postBodyData appendData:data];
}

- (void)setPostBody:(NSMutableData*)data
{
    self.postBodyData = data;
}

- (void)addPostValue:(NSString *)value forKey:(NSString *)key
{
    if (!self.postData)
    {
        self.postData = [NSMutableArray array];
    }
    
    NSMutableDictionary *keyValuePair = [NSMutableDictionary dictionaryWithCapacity:2];
    [keyValuePair setValue:key forKey:@"key"];
    [keyValuePair setValue:[value dataUsingEncoding:NSUTF8StringEncoding] forKey:@"value"];
    [self.postData addObject:keyValuePair];
}

- (void)setPostValue:(NSString *)value forKey:(NSString *)key
{
    // remove any existing value
    NSUInteger i;
    for (i=0; i<self.postData.count; i++)
    {
        NSDictionary *val = [self.postData objectAtIndex:i];
        if ([[val objectForKey:@"key"] isEqualToString:key])
        {
            [self.postData removeObjectAtIndex:i];
            i--;
        }
    }
    
    [self addPostValue:value forKey:key];
}

- (void)replacePostValue:(NSString *)value forKey:(NSString *)key
{
    // update existing value
    for (NSMutableDictionary *val in [self postData])
    {
        if ([[val objectForKey:@"key"] isEqualToString:key])
        {
            [val setValue:[value description] forKey:@"value"];
            break;
        }
    }
}

- (void)addFile:(NSString *)filePath forKey:(NSString *)key
{
    [self addFile:filePath withFileName:nil andContentType:nil forKey:key];
}

- (void)addFile:(NSString *)filePath withFileName:(NSString *)fileName andContentType:(NSString *)contentType forKey:(NSString *)key
{
    if (!fileName)
    {
        fileName = [filePath lastPathComponent];
    }
    
    if (!contentType)
    {
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
        {
            NSString *fileExtension = [filePath pathExtension];
            NSString *UTI = (__bridge_transfer NSString *)UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (__bridge CFStringRef)fileExtension, NULL);
            contentType = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)UTI, kUTTagClassMIMEType);
        }
    }
    
    [self addData:filePath withFileName:fileName andContentType:contentType forKey:key];
}

- (void)setFile:(NSString *)filePath forKey:(NSString *)key
{
    [self setFile:filePath withFileName:nil andContentType:nil forKey:key];
}

- (void)setFile:(id)data withFileName:(NSString *)fileName andContentType:(NSString *)contentType forKey:(NSString *)key
{
    // remove any existing value
    NSUInteger i;
    for (i=0; i<self.fileData.count; i++)
    {
        NSDictionary *val = [self.fileData objectAtIndex:i];
        if ([[val objectForKey:@"key"] isEqualToString:key])
        {
            [self.fileData removeObjectAtIndex:i];
            i--;
        }
    }
    
    [self addFile:data withFileName:fileName andContentType:contentType forKey:key];
}

- (void)addData:(NSData *)data forKey:(NSString *)key
{
    [self addData:data withFileName:@"file" andContentType:nil forKey:key];
}

- (void)addData:(id)data withFileName:(NSString *)fileName andContentType:(NSString *)contentType forKey:(NSString *)key
{
    if (!self.fileData)
    {
        self.fileData = [NSMutableArray array];
    }
    
    if (!contentType)
    {
        contentType = @"application/octet-stream";
    }
    
    NSMutableDictionary *fileInfo = [NSMutableDictionary dictionaryWithCapacity:4];
    [fileInfo setValue:key forKey:@"key"];
    [fileInfo setValue:fileName forKey:@"fileName"];
    [fileInfo setValue:contentType forKey:@"contentType"];
    [fileInfo setValue:data forKey:@"data"];
    [self.fileData addObject:fileInfo];
}

- (void)setData:(NSData *)data forKey:(NSString *)key
{
    [self setData:data withFileName:@"file" andContentType:nil forKey:key];
}

- (void)setData:(id)data withFileName:(NSString *)fileName andContentType:(NSString *)contentType forKey:(NSString *)key
{
    // remove any existing value
    NSUInteger i;
    for (i=0; i<self.fileData.count; i++)
    {
        NSDictionary *val = [self.fileData objectAtIndex:i];
        if ([[val objectForKey:@"key"] isEqualToString:key])
        {
            [self.fileData removeObjectAtIndex:i];
            i--;
        }
    }
    
    [self addData:data withFileName:fileName andContentType:contentType forKey:key];
}

- (NSString *)responseString
{
    NSString *result = nil;
    NSData *data = [self responseData];
    if (data.length)
    {
        result = [[NSString alloc] initWithBytes:[data bytes] length:[data length] encoding:[self responseEncoding]];
    }
    return result;
}

- (BOOL)isResponseCompressed
{
    BOOL result = NO;
    NSString *encoding = [[self responseHeaders] objectForKey:@"Content-Encoding"];
    if (encoding && [encoding rangeOfString:@"gzip"].location != NSNotFound)
    {
        result = YES;
    }
    return result;
}

- (NSData *)responseData
{
    return [self rawResponseData];
}

- (void)startSynchronous
{
    int isAbort = 0;
    [self startSynchronousAbort:&isAbort];
}

- (void)startSynchronousAbort:(int*)isAbort
{
    if (![self isCancelled] && ![self isFinished])
    {
        isSynchronous = YES;
        
        [self main];
        
        while (![self isFinished] && (*isAbort == 0)) {
            @autoreleasepool {
                [[NSRunLoop currentRunLoop] runMode:AFHTTPRequestRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:10]];
            }
        }
    }
}

- (void)startAsynchronous
{
    if (!sharedQueue)
    {
        sharedQueue = [[NSOperationQueue alloc] init];
        [sharedQueue setMaxConcurrentOperationCount:2];
    }
    [sharedQueue addOperation:self];
}

- (void)clearDelegatesAndCancel
{
    self.delegate = nil;
    self.startedBlock = nil;
    self.headersReceivedBlock = nil;
    self.completeBlock = nil;
    self.failedBlock = nil;
    self.bytesReceivedBlock = nil;
    self.bytesSentBlock = nil;
    self.dataReceivedBlock = nil;
    [self cancel];
}

- (void)setStartedBlock:(AFOPBasicBlock)aStartedBlock
{
    _startedBlock = [aStartedBlock copy];
}

- (void)setHeadersReceivedBlock:(AFOPHeadersBlock)aHeadersReceivedBlock
{
    _headersReceivedBlock = [aHeadersReceivedBlock copy];
}

- (void)setCompletionBlock:(AFOPBasicBlock)aCompletionBlock
{
    _completeBlock = [aCompletionBlock copy];
}

- (void)setFailedBlock:(AFOPBasicBlock)aFailedBlock
{
    _failedBlock = [aFailedBlock copy];
}

- (void)setBytesReceivedBlock:(AFOPProgressBlock)aBytesReceivedBlock
{
    _bytesReceivedBlock = [aBytesReceivedBlock copy];
}

- (void)setBytesSentBlock:(AFOPProgressBlock)aBytesSentBlock
{
    _bytesSentBlock = [aBytesSentBlock copy];
}

- (void)setDataReceivedBlock:(AFOPDataBlock)aDataReceivedBlock
{
    _dataReceivedBlock = [aDataReceivedBlock copy];
}

#pragma mark - NSOperation methods

- (void)start
{
    if ([self isCancelled])
    {
        [self buildRequestFailed:AFRequestCancelledErrorType info:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"request canceled [url:%@]", self.url], NSLocalizedDescriptionKey, nil]];
        return;
    }

    self.executing = YES;

    [self main];
}

- (void)main
{
    @try {
#if TARGET_OS_IPHONE
        if ([self shouldContinueWhenAppEntersBackground])
        {
            if (!backgroundTask || backgroundTask == UIBackgroundTaskInvalid) {
                backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{
                    // Synchronize the cleanup call on the main thread in case
                    // the task actually finishes at around the same time.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (backgroundTask != UIBackgroundTaskInvalid)
                        {
                            [[UIApplication sharedApplication] endBackgroundTask:backgroundTask];
                            backgroundTask = UIBackgroundTaskInvalid;
                            self.canceledByBackgroundExpiration = YES;
                            [self cancel];
                        }
                    });
                }];
            }
        }
#endif
        if (![self url])
        {
            [self buildRequestFailed:AFUnableToCreateRequestErrorType info:[NSDictionary dictionaryWithObjectsAndKeys:@"unable to create request (bad url?)", NSLocalizedDescriptionKey, nil]];
            return;
        }
        
        // implement method [checkServerStatus] in QNAPBaseCalss "ASIHTTPRequest+Utility.m"
        BOOL continueRequest = YES;
        SEL selector = NSSelectorFromString(@"checkServerStatus");
        if ([self respondsToSelector:selector])
        {
            Method method = class_getInstanceMethod([self class], selector);
            IMP imp = method_getImplementation(method);
            continueRequest = ((BOOL (*)(id, SEL))imp)(self, selector);
        }
        if (continueRequest)
        {
            [self continueMain];
        }
    } @catch (NSException *exception) {
        [self buildRequestFailed:AFUnhandledExceptionError info:[NSDictionary dictionaryWithObjectsAndKeys:[exception name], NSLocalizedDescriptionKey, [exception reason], NSLocalizedFailureReasonErrorKey,nil]];
    } @finally {
    }
}

- (void)cancel
{
    if (self.PACFileRequest)
    {
        [self.PACFileRequest cancel];
        [self setPACFileRequest:nil];
    }
    [self.task cancel];
    [super cancel];
}

- (BOOL)isAsynchronous
{
    return YES;
}

- (BOOL)isExecuting
{
    @synchronized(self) {
        return _executing;
    }
}

- (BOOL)isFinished
{
    @synchronized(self) {
        return _finished;
    }
}

@end
