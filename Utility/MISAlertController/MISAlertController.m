//
//  MISAlertController.m
//
//  Created by Michael Schneider on 9/28/14.
//  Copyright (c) 2014 Michael Schneider. Licensed under the MIT license.
//

#import "MISAlertController.h"

static BOOL supportDebugLog = NO;

#define MISAlertLog(fmt, ...) { if (supportDebugLog) { NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__); }}

#pragma mark - MISAlertAction

@interface MISAlertAction ()
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) UIAlertActionStyle style;
@property (nonatomic, copy) void (^handler)(MISAlertAction *action);
@end

@implementation MISAlertAction

- (void)dealloc
{
    MISAlertLog(@"dealloc [p:%p]", self);
}

#pragma mark Class

+ (instancetype)actionWithTitle:(NSString *)title style:(UIAlertActionStyle)style handler:(void (^)(MISAlertAction *action))handler
{
    return [[MISAlertAction alloc] initWithTitle:title style:style handler:handler];
}


#pragma mark Lifecycle

- (instancetype)initWithTitle:(NSString *)title style:(UIAlertActionStyle)style handler:(void (^)(MISAlertAction *action))handler
{
    self = [super init];
    if (self == nil) { return nil; }
    
    _title = title;
    _style = style;
    _handler = handler;
    
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    MISAlertAction *alertAction = [self.class new];
    alertAction.title = self.title;
    alertAction.style = self.style;
    alertAction.handler = self.handler;
    return alertAction;
}

@end


#pragma mark - MISUIAlertController

@interface MISUIAlertController ()
@property (nonatomic, copy) void (^didDisappearCallback)(void);
@property (nonatomic, strong) NSMutableArray *internalActions;
@end

@implementation MISUIAlertController

- (void)dealloc
{
    MISAlertLog(@"dealloc [p:%p]", self);
}

#pragma mark UIViewController

- (void)viewDidDisappear:(BOOL)animated
{
    MISAlertLog(@"viewDidDisappear [p:%p]", self);
    
    [super viewDidDisappear:animated];
    
    if (self.contentViewController)
    {
        for (UIView *subView in self.contentViewController.view.subviews)
        {
            if ([subView isKindOfClass:[UIActivityIndicatorView class]])
            {
                UIActivityIndicatorView *spinner = (UIActivityIndicatorView*)subView;
                [spinner stopAnimating];
            }
            [subView removeFromSuperview];
        }
        self.contentViewController = nil;
    }
    
    if (self.didDisappearCallback) {
        self.didDisappearCallback();
    }
}

#pragma mark UIAlertController

- (void)addAction:(MISAlertAction *)action
{
    NSAssert([action isKindOfClass:MISAlertAction.class], @"Must be of type MISAlertAction");
    
    if (self.internalActions == nil)
    {
        self.internalActions = [NSMutableArray array];
    }
    [self.internalActions addObject:action];
    
    if (action.style == UIAlertActionStyleCancel)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && self.preferredStyle == UIAlertControllerStyleActionSheet)
        {
            action.style = UIAlertActionStyleDefault;
        }
    }
    
    __weak MISUIAlertController *tempSelf = self;
    __weak MISAlertAction *tempAction = action;
    
    UIAlertAction *alertAction = [UIAlertAction actionWithTitle:action.title style:(UIAlertActionStyle)action.style handler:^(UIAlertAction *uiAction) {
        id appdelegate= [[UIApplication sharedApplication] delegate];
        
        SEL updateSelector = NSSelectorFromString(@"updatePopupMsg:op:");
        if ([appdelegate respondsToSelector:updateSelector])
        {
            IMP imp = [appdelegate methodForSelector:updateSelector];
            void(*func)(id, SEL, id, NSNumber*) = (void *)imp;
            func(appdelegate, updateSelector, tempSelf, [NSNumber numberWithBool:NO]);
        }
        
        SEL selector = NSSelectorFromString(@"hideAlertWindow");
        if ([appdelegate respondsToSelector:selector])
        {
            IMP imp = [appdelegate methodForSelector:selector];
            void(*func)(id, SEL) = (void *)imp;
            func(appdelegate, selector);
        }
        
        if (tempAction.handler)
        {
            tempAction.handler(tempAction);
        }
        
        [tempSelf.internalActions removeAllObjects];
    }];
    [super addAction:alertAction];
}

- (void)addSpinner
{
    UIViewController *customVC = [[UIViewController alloc] init];
    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.color = [UIColor blackColor];
    spinner.backgroundColor = [UIColor clearColor];
    [spinner startAnimating];
    [customVC.view addSubview:spinner];
    NSLayoutConstraint *myConstraint = nil;
    // center X
    myConstraint = [NSLayoutConstraint constraintWithItem:spinner attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:[spinner superview] attribute:NSLayoutAttributeCenterX multiplier:1.0f constant:0.0f];
    [[spinner superview] addConstraint:myConstraint];
    // y position
    myConstraint = [NSLayoutConstraint constraintWithItem:spinner attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:[spinner superview] attribute:NSLayoutAttributeCenterY multiplier:1.0f constant:-10.0f];
    [[spinner superview] addConstraint:myConstraint];
    self.contentViewController = customVC;
}

- (void)removeAllActions
{
    for (id act in self.internalActions)
    {
        MISAlertLog(@"remove action [ui:%p][action:%p]", self, act);
    }
    [self.internalActions removeAllObjects];
}

#pragma mark Show

- (void)showFromSourceView:(UIView *)sourceView inViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    UIPopoverPresentationController *popover = self.popoverPresentationController;
    if (popover) {
        // If we have a source view use this to layout the popover
        if (sourceView != nil) {
            popover.sourceView = sourceView;
            popover.sourceRect = sourceView.bounds;
        }
        else {
            // If no source view is available show the action sheet centered
            popover.sourceView = viewController.view;
            popover.sourceRect = viewController.view.bounds;
            popover.permittedArrowDirections = 0;
        }
    }
    
    [viewController presentViewController:self animated:animated completion:nil];
}

@end


#pragma mark - MISAlertControllerHelper

@interface MISAlertControllerHelper ()
@property (nonatomic, assign) UIAlertControllerStyle preferredStyle;
@property (nonatomic, strong) NSMutableArray *internalActions;

- (void)addAction:(MISAlertAction *)action;
@property (nonatomic, readonly) NSArray *actions;
- (void)addTextFieldWithConfigurationHandler:(void (^)(UITextField *textField))configurationHandler;
@property (nonatomic, readonly) NSArray *textFields;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *message;

@property (nonatomic, copy) void (^didDisappearCallback)(void);
- (void)configureControl:(id)control;

- (void)showFromSourceView:(UIView *)sourceView inViewController:(UIViewController *)viewController animated:(BOOL)animated;
@end

@interface MISAlertControllerAlertViewHelper : MISAlertControllerHelper<UIAlertViewDelegate>
@property (strong, nonatomic) UIAlertView *alertView;
@end

@interface MISAlertControllerActionSheetHelper : MISAlertControllerHelper<UIActionSheetDelegate>
@property (nonatomic, strong) UIActionSheet *actionSheet;
@end

@implementation MISAlertControllerHelper

- (void)dealloc
{
    MISAlertLog(@"dealloc [p:%p]", self);
}

#pragma mark Class

+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(UIAlertControllerStyle)preferredStyle
{
    MISAlertControllerHelper *helper = (preferredStyle == UIAlertControllerStyleActionSheet) ? MISAlertControllerActionSheetHelper.new : MISAlertControllerAlertViewHelper.new;
    helper.title = title;
    helper.message = message;
    helper.preferredStyle = preferredStyle;
    return helper;
}

#pragma mark Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self == nil) { return self; }
    _internalActions = [NSMutableArray array];
    return self;
}

#pragma mark Setter / Getter

- (id)control
{
    // Subclass should return either UIAlertView or UIActionSheet
    return nil;
}

- (void)setTitle:(NSString *)title
{
    id control = self.control;
    if ([control respondsToSelector:@selector(setTitle:)]) {
        [control setTitle:title];
    }
}

- (NSString *)title
{
    id control = self.control;
    if ([control respondsToSelector:@selector(title)]) {
        return [control title];
    }
    return nil;
}

- (void)setMessage:(NSString *)message
{
    id control = self.control;
    if ([control respondsToSelector:@selector(setMessage:)]) {
        [control setMessage:message];
    }
}

- (NSString *)message
{
    id control = self.control;
    if ([control respondsToSelector:@selector(message)]) {
        return [control message];
    }
    
    return nil;
}

#pragma mark Actions

- (void)addAction:(MISAlertAction *)action
{
    // Subclass needs to implement
}

- (NSArray *)actions
{
    return [self.internalActions copy];
}

#pragma mark TextFields

- (void)addTextFieldWithConfigurationHandler:(void (^)(UITextField *textField))configurationHandler
{
    // Subclass needs to implement
}

- (NSArray *)textFields
{
    return @[];
}

#pragma mark ActivityIndicatorView

- (void)addSpinner
{
    // Subclass needs to implement
}

- (void)removeAllActions
{
    // Subclass needs to implement
}

#pragma mark Configuration

- (void)configureControl:(id)control
{
    // Add all actions to the control
    [self.internalActions enumerateObjectsUsingBlock:^(MISAlertAction *action, NSUInteger idx, BOOL *stop) {
        NSInteger buttonIdx = [control addButtonWithTitle:action.title];
        if (action.style == UIAlertActionStyleCancel) {
            if ([control respondsToSelector:@selector(setCancelButtonIndex:)]) {
                [control setCancelButtonIndex:buttonIdx];
            }
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && self.preferredStyle == UIAlertControllerStyleActionSheet)
            {
                [control addButtonWithTitle:action.title];
            }
        }
        else if (action.style == UIAlertActionStyleDestructive) {
            if ([control respondsToSelector:@selector(setDestructiveButtonIndex:)]) {
                [control setDestructiveButtonIndex:buttonIdx];
            }
        }
    }];
}

#pragma mark UIActionSheet / UIAlertView Delegate helper

- (void)clickedButtonAtIndex:(NSInteger)buttonIndex
{
    id appdelegate= [[UIApplication sharedApplication] delegate];
    
    SEL updateSelector = NSSelectorFromString(@"updatePopupMsg:op:");
    if ([appdelegate respondsToSelector:updateSelector])
    {
        IMP imp = [appdelegate methodForSelector:updateSelector];
        void(*func)(id, SEL, id, NSNumber*) = (void *)imp;
        func(appdelegate, updateSelector, self, [NSNumber numberWithBool:NO]);
    }
    
    SEL selector = NSSelectorFromString(@"hideAlertWindow");
    if ([appdelegate respondsToSelector:selector])
    {
        IMP imp = [appdelegate methodForSelector:selector];
        void(*func)(id, SEL) = (void *)imp;
        func(appdelegate, selector);
    }
    
    MISAlertAction *alertAction = self.internalActions[buttonIndex];
    void (^callback)(MISAlertAction *action) = alertAction.handler;
    if (callback) {
        callback(self.actions[buttonIndex]);
    }
}

- (void)didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (self.didDisappearCallback) {
        self.didDisappearCallback();
    }
}

#pragma mark Show

- (void)showFromSourceView:(UIView *)sourceView inViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    // Show alert view or action sheet
    id control = self.control;
    
    [self configureControl:control];
    
    if (sourceView != nil && [control respondsToSelector:@selector(showFromRect:inView:animated:)]) {
        [control showFromRect:sourceView.bounds inView:sourceView animated:animated];
    }
    else if ([control respondsToSelector:@selector(showInView:)]) {
        [control showInView:viewController.view];
    }
    else if ([control respondsToSelector:@selector(show)]) {
        [control show];
    }
}

#pragma mark Hide

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    id control = self.control;
    
    if ([control respondsToSelector:@selector(dismissWithClickedButtonIndex:animated:)] &&
        [control respondsToSelector:@selector(isVisible)] && [control isVisible]) {
        [control dismissWithClickedButtonIndex:-1 animated:YES];
    }
    
    if (completion) {
        completion();
    }
}

@end


#pragma mark - MISAlertControllerAlertViewHelper

@implementation MISAlertControllerAlertViewHelper

- (void)dealloc
{
    MISAlertLog(@"dealloc [p:%p]", self);
}

#pragma mark Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self == nil) { return self; }
    _alertView = [[UIAlertView alloc] init];
    _alertView.delegate = self;
    return self;
}

#pragma mark MISAlertControllerHelper

- (id)control
{
    return self.alertView;
}

- (void)addAction:(MISAlertAction *)action
{
    [self.internalActions addObject:action];
}

- (void)addTextFieldWithConfigurationHandler:(void (^)(UITextField *textField))configurationHandler
{
    // Add text field to alert view and return the text field to the configuration handler
    self.alertView.alertViewStyle = UIAlertViewStylePlainTextInput;
    if (configurationHandler) {
        configurationHandler([self.alertView textFieldAtIndex:0]);
    }
}

- (NSArray *)textFields
{
    if (self.alertView.alertViewStyle == UIAlertViewStylePlainTextInput) {
        return @[[self.alertView textFieldAtIndex:0]];
    }
    
    return [super textFields];
}

- (void)addSpinner
{
}

- (void)removeAllActions
{
}

#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *) alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self clickedButtonAtIndex:buttonIndex];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self didDismissWithButtonIndex:buttonIndex];
}

@end


#pragma mark - MISAlertControllerActionSheetHelper

@implementation MISAlertControllerActionSheetHelper

- (void)dealloc
{
    MISAlertLog(@"dealloc [p:%p]", self);
}

- (instancetype)init
{
    self = [super init];
    if (self == nil) { return self; }
    _actionSheet = [[UIActionSheet alloc] init];
    _actionSheet.delegate = self;
    return self;
}


#pragma mark MISAlertControllerHelper

- (id)control
{
    return self.actionSheet;
}

- (void)addAction:(MISAlertAction *)action
{
    //[self.internalActions insertObject:action atIndex:0];
    [self.internalActions addObject:action];
}

- (void)addTextFieldWithConfigurationHandler:(void (^)(UITextField *textField))configurationHandler
{
    NSAssert(NO, @"MISAlertControllerStyleActionSheet does not support text fields");
}

- (void)addSpinner
{
    NSAssert(NO, @"MISAlertControllerStyleActionSheet does not support spinner");
}

- (void)removeAllActions
{
    NSAssert(NO, @"MISAlertControllerStyleActionSheet does not support remove all actions");
}

#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self clickedButtonAtIndex:buttonIndex];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    [self didDismissWithButtonIndex:buttonIndex];
}

@end


#pragma mark - MISAlertController

@interface MISAlertController () <UIActionSheetDelegate, UIAlertViewDelegate>
@property (nonatomic, strong) id alertController;
@property (nonatomic, strong) NSTimer *delayDismissTimer;
@property (nonatomic, assign) int delayRetryCount;
@end

@implementation MISAlertController

- (void)dealloc
{
    MISAlertLog(@"dealloc [p:%p]", self);
}

#pragma mark Class

+ (void)setSupportDebugLog:(BOOL)support
{
    supportDebugLog = support;
}

+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(UIAlertControllerStyle)preferredStyle
{
    return [[MISAlertController alloc] initWithTitle:title message:message preferredStyle:preferredStyle];
}

#pragma mark Lifecycle

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(UIAlertControllerStyle)preferredStyle
{
    self = [super init];
    if (self == nil) { return self; }
    [self configureControllerWithTitle:title message:message preferredStyle:preferredStyle];
    
    return self;
}

#pragma mark Control

- (void)configureControllerWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(UIAlertControllerStyle)preferredStyle
{
    if (NSClassFromString(@"UIAlertController") != nil) {
        self.alertController = [MISUIAlertController alertControllerWithTitle:title message:message preferredStyle:preferredStyle];
    } else {
        self.alertController = [MISAlertControllerHelper alertControllerWithTitle:title message:message preferredStyle:preferredStyle];
    }
}

#pragma mark Show Alert Controller

- (UIViewController *)topMostController
{
    UIViewController *topController = nil;
    
    id appdelegate= [[UIApplication sharedApplication] delegate];
    SEL selector = NSSelectorFromString(@"showAlertWindow");
    if ([appdelegate respondsToSelector:selector])
    {
        IMP imp = [appdelegate methodForSelector:selector];
        id (*func)(id, SEL) = (void *)imp;
        topController = (UIViewController*)func(appdelegate, selector);
    }
    else
    {
        topController = [[UIApplication sharedApplication] keyWindow].rootViewController;
        
        while (topController.presentedViewController) {
            topController = topController.presentedViewController;
        }
    }
    
    return topController;
}

- (void)showInTopMostViewControllerWithAnimated:(BOOL)animated
{
    UIViewController *topMostViewController = [self topMostController];
    [self showFromSourceView:nil inViewController:topMostViewController animated:animated];
}

- (void)showInViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [self showFromSourceView:nil inViewController:viewController animated:animated];
}

- (void)showFromSourceView:(UIView *)sourceView inViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    // Show alert view or action sheet
    id alertController = self.alertController;

    // Use __block in this case so self is not dealloced before the control disappeared
    __block MISAlertController *blockSelf = self;
    [alertController setDidDisappearCallback:^{
        // Push it to the next run loop so we dealloc self after the action was fired
        dispatch_async(dispatch_get_main_queue(), ^{
            id appdelegate= [[UIApplication sharedApplication] delegate];
            SEL updateSelector = NSSelectorFromString(@"updatePopupMsg:op:");
            if ([appdelegate respondsToSelector:updateSelector])
            {
                IMP imp = [appdelegate methodForSelector:updateSelector];
                void(*func)(id, SEL, id, NSNumber*) = (void *)imp;
                func(appdelegate, updateSelector, blockSelf.alertController, [NSNumber numberWithBool:NO]);
            }
            
            blockSelf.alertController = nil;
            blockSelf = nil;
        });
    }];
    
    [alertController showFromSourceView:sourceView inViewController:viewController animated:animated];
    
    id appdelegate= [[UIApplication sharedApplication] delegate];
    SEL updateSelector = NSSelectorFromString(@"updatePopupMsg:op:");
    if ([appdelegate respondsToSelector:updateSelector])
    {
        IMP imp = [appdelegate methodForSelector:updateSelector];
        void(*func)(id, SEL, id, NSNumber*) = (void *)imp;
        func(appdelegate, updateSelector, self.alertController, [NSNumber numberWithBool:YES]);
    }
}

#pragma mark Hide Alert Controller

- (void)delayToDismissViewController
{
    BOOL animated = [[self.delayDismissTimer.userInfo objectForKey:@"animated"] boolValue];
    void (^completion)(void) = [self.delayDismissTimer.userInfo objectForKey:@"completion"];
    
    if (self.delayDismissTimer)
    {
        [self.delayDismissTimer invalidate];
        self.delayDismissTimer = nil;
    }
    
    if (completion)
    {
        [self dismissViewControllerAnimated:animated completion:completion];
        completion = nil;
    }
}

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion
{
    int maxRelayRetryCount = 30;// 0.1*30: 3sec
    BOOL delayDismiss = NO;
    if ([self.alertController isKindOfClass:[UIViewController class]])
    {
        UIViewController *tempVC = (UIViewController*)self.alertController;
        if (!tempVC.presentingViewController)
        {
            delayDismiss = YES;
        }
    }
    MISAlertLog(@"[ctr:%p][ui:%p][delay:%d]", self, self.alertController, delayDismiss);
    
    if (delayDismiss && self.delayRetryCount < maxRelayRetryCount)
    {
        self.delayRetryCount += 1;
        CGFloat delayInSeconds = 0.1;
        NSDictionary *info = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:flag], @"animated", [completion copy], @"completion", nil];
        
        self.delayDismissTimer = [NSTimer timerWithTimeInterval:delayInSeconds target:self selector:@selector(delayToDismissViewController) userInfo:info repeats:NO];
        [[NSRunLoop mainRunLoop] addTimer:self.delayDismissTimer forMode:NSDefaultRunLoopMode];
        return;
    }
    
    id appdelegate= [[UIApplication sharedApplication] delegate];
    
    SEL updateSelector = NSSelectorFromString(@"updatePopupMsg:op:");
    if ([appdelegate respondsToSelector:updateSelector])
    {
        IMP imp = [appdelegate methodForSelector:updateSelector];
        void(*func)(id, SEL, id, NSNumber*) = (void *)imp;
        func(appdelegate, updateSelector, self.alertController, [NSNumber numberWithBool:NO]);
    }
    
    SEL selector = NSSelectorFromString(@"hideAlertWindow");
    if ([appdelegate respondsToSelector:selector])
    {
        IMP imp = [appdelegate methodForSelector:selector];
        void(*func)(id, SEL) = (void *)imp;
        func(appdelegate, selector);
    }
    
    if (self.alertController)
    {
        [self.alertController removeAllActions];
        [self.alertController dismissViewControllerAnimated:flag completion:^{
            if (completion) {
                completion();
            }
        }];
    }
    else
    {
        // If alertController is already dismissed just call completion block
        if (completion) {
            completion();
        }
    }
}

#pragma mark Forwarding

- (id)forwardingTargetForSelector:(SEL)selector
{
    // Dispatch all messages from the Proxy category to the alert controller
    if ([self.alertController respondsToSelector:selector]) {
        return self.alertController;
    }
    
    return [super forwardingTargetForSelector:selector];
}

@end
