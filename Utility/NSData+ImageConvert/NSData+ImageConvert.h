//
// NSData+ImageConvert.h
//
// Copyright (c) 2017 QNAP Systems, Inc.
// All Rights Reserved.
//
// Developed by QNAP Systems.
//

@interface NSData (ImageConvert)

- (NSData*)imageDataConvertToJPGData;

@end
