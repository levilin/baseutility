//
// NSData+ImageConvert.m
//
// Copyright (c) 2017 QNAP Systems, Inc.
// All Rights Reserved.
//
// Developed by QNAP Systems.
//

#import "NSData+ImageConvert.h"
#import <ImageIO/ImageIO.h>
#import <MobileCoreServices/MobileCoreServices.h>

@implementation NSData (ImageConvert)

- (NSData*)imageDataConvertToJPGData
{
    CGImageSourceRef source = CGImageSourceCreateWithData((__bridge CFDataRef)self, NULL);
    CGImageRef imgRef = CGImageSourceCreateImageAtIndex(source, 0, nil);
    
    CFStringRef UTI = kUTTypeJPEG;
    NSMutableData *destData = [NSMutableData data];
    CGImageDestinationRef destination = CGImageDestinationCreateWithData((__bridge CFMutableDataRef)destData, UTI, 1, NULL);
    if (!destination)
    {
        // failed to create image destination
    }
    
    // copy all metadata in source to destination
    CGImageDestinationAddImage(destination, imgRef, CGImageSourceCopyPropertiesAtIndex(source, 0, NULL));
    BOOL success = CGImageDestinationFinalize(destination);
    if (!success)
    {
        // failed to create data from image destination
    }
    
    if (destination)
    {
        CFRelease(destination);
    }
    if (source)
    {
        CFRelease(source);
    }
    if (imgRef)
    {
        CFRelease(imgRef);
    }
    
    return destData;
}

@end
