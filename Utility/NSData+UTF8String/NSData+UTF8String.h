//
// NSData+UTF8String.h
//
// Copyright (c) 2014 QNAP Systems, Inc.
// All Rights Reserved.
//
// Developed by QNAP Systems.
//

@interface NSData (UTF8String)

- (NSString*)UTF8String;
- (NSData*)dataByHealingUTF8Stream;

@end
