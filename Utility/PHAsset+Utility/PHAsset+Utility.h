//
// PHAsset+Utility.h
//
// Copyright (c) 2018 QNAP Systems, Inc.
// All Rights Reserved.
//
// Developed by QNAP Systems, Inc.
//

#import <Photos/Photos.h>

@interface PHAsset (Utility)

- (BOOL)checkForAdjustment:(BOOL)networkAccessAllowed;
- (NSDictionary*)getAssetImage:(NSString*)fileName networkAccessAllowed:(BOOL)networkAccessAllowed version:(PHImageRequestOptionsVersion)version;

@end
