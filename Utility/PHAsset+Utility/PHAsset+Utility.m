//
// PHAsset+Utility.m
//
// Copyright (c) 2018 QNAP Systems, Inc.
// All Rights Reserved.
//
// Developed by QNAP Systems, Inc.
//

#import "PHAsset+Utility.h"
#import <MobileCoreServices/MobileCoreServices.h>

@implementation PHAsset (Utility)

- (BOOL)checkForAdjustment:(BOOL)networkAccessAllowed
{
    __block BOOL hasAdjustment = NO;
    
    NSConditionLock *conditionLock = [[NSConditionLock alloc] initWithCondition:1];
    
    PHContentEditingInputRequestOptions *options = [[[PHContentEditingInputRequestOptions alloc] init] autorelease];
    options.networkAccessAllowed = networkAccessAllowed;
    options.canHandleAdjustmentData = ^BOOL(PHAdjustmentData *adjustmentData) {
        return YES;
    };
    
    [self requestContentEditingInputWithOptions:options completionHandler:^(PHContentEditingInput *contentEditingInput, NSDictionary *info) {
        // callback on main thread
        hasAdjustment = contentEditingInput.adjustmentData != nil;
        
        [conditionLock lock];
        [conditionLock unlockWithCondition:0];
    }];
    
    [conditionLock lockWhenCondition:0];
    [conditionLock unlock];
    [conditionLock release];
    conditionLock = nil;
    
    return hasAdjustment;
}

- (NSDictionary*)getAssetImage:(NSString*)fileName networkAccessAllowed:(BOOL)networkAccessAllowed version:(PHImageRequestOptionsVersion)version
{
    __block NSMutableDictionary *infoDict = [NSMutableDictionary dictionary];
    
    NSConditionLock *conditionLock = [[NSConditionLock alloc] initWithCondition:1];
    
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    PHImageRequestOptions *option = [[[PHImageRequestOptions alloc] init] autorelease];
    option.networkAccessAllowed = networkAccessAllowed;
    option.synchronous = NO;
    option.version = version;
    
    [[PHImageManager defaultManager] requestImageDataForAsset:self options:option resultHandler:^(NSData *imageData, NSString *dataUTI, UIImageOrientation orientation, NSDictionary *info) {
        NSURL *fileURL = [info objectForKey:@"PHImageFileURLKey"];
        if (fileURL)
        {
            NSString *dataFileName = [fileURL lastPathComponent];
            if (dataFileName.length)
            {
                [infoDict setObject:dataFileName forKey:@"imageDataName"];
            }
        }
        else
        {
            NSString *oriUTI = [NSMakeCollectable(UTTypeCreatePreferredIdentifierForTag(kUTTagClassFilenameExtension, (CFStringRef)[fileName pathExtension], NULL)) autorelease];
            NSString *dataUTI = [info objectForKey:@"PHImageFileUTIKey"];
            if (oriUTI && dataUTI && [oriUTI caseInsensitiveCompare:dataUTI] != NSOrderedSame)
            {
                // not the same with the original fileName uti.
                if ([[dataUTI pathExtension] caseInsensitiveCompare:@"jpeg"] == NSOrderedSame)
                {
                    NSString *extString = @"jpg";
                    NSString *dataFileName = [NSString stringWithFormat:@"%@.%@", [fileName stringByDeletingPathExtension], extString];
                    [infoDict setObject:dataFileName forKey:@"imageDataName"];
                }
            }
        }
        
        if (imageData)
        {
            [infoDict setObject:imageData forKey:@"imageData"];
        }
        
        [conditionLock lock];
        [conditionLock unlockWithCondition:0];
    }];
    
    [pool release];
    
    [conditionLock lockWhenCondition:0];
    [conditionLock unlock];
    [conditionLock release];
    conditionLock = nil;
    
    return infoDict;
}

@end
